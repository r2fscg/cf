use cf::aperedis::ApeRedis;
use cf::log::{info, warn};
use redis;

fn init_redis_list(size: i32) {
    let host = "mik.vxtrans.com";
    let port = 1216;
    let password = "jSjInAwArNzeApQJqNy9jqNQ3n2rftuN";
    let mut rd = ApeRedis::new(host, port, password);
    let mut cli = rd.client;
    let list_name = "mylist";
    redis::cmd("ltrim")
        .arg(list_name)
        .arg(0)
        .arg(10)
        .query::<String>(&mut cli);
    while true {
        let len = redis::cmd("LLEN").arg(list_name).query::<i32>(&mut cli);
        if len.is_err() {
            warn(format!("{:?}", len.err()).as_str());
            break;
        }
        let len = len.unwrap();
        info(&format!("current length is {:?}", len));
        let diff = size - len;
        info(format!("diff {:?}", diff).as_str());
        if diff > 0 {
            let mut pipe = redis::pipe();
            for i in 0..diff {
                pipe.atomic()
                    .cmd("LPUSH")
                    .arg(list_name)
                    .arg(format!("{}", i).as_str());
            }
            let _ = pipe.query::<()>(&mut cli);
        } else {
            info("list is full");
        }
        info("mission complete");
        break;
    }
}

fn main() {
    info("Hello, world!");
    let x = 1;
    init_redis_list(1000);
}
