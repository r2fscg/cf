#[derive(Default)]
pub struct Trie {
    is_end: bool,
    nodes: [Option<Box<Trie>>; 26],
}

/**
 * `&self` means the method takes an immutable reference.
 * If you need a mutable reference, change it to `&mut self` instead.
 */
impl Trie {
    /** Initialize your data structure here. */
    pub fn new() -> Self {
        Default::default()
    }

    /** Inserts a word into the trie. */
    pub fn insert(&mut self, word: String) {
        let mut curr = self;
        for i in word.chars().map(|char| (char as u8 - 'a' as u8) as usize) {
            curr = curr.nodes[i].get_or_insert_with(|| Box::new(Trie::new()));
        }
        curr.is_end = true;
    }

    /** Returns if the word is in the trie. */
    pub fn search(&self, word: String) -> bool {
        let mut curr = self;
        for i in word.chars().map(|char| (char as u8 - 'a' as u8) as usize) {
            match curr.nodes[i].as_ref() {
                Some(node) => {
                    curr = node;
                }
                None => {
                    return false;
                }
            }
        }
        curr.is_end
    }

    /** Returns if there is any word in the trie that starts with the given prefix. */
    pub fn starts_with(&self, prefix: String) -> bool {
        let mut curr = self;
        for i in prefix.chars().map(|char| (char as u8 - 'a' as u8) as usize) {
            match curr.nodes[i].as_ref() {
                Some(node) => {
                    curr = node;
                }
                None => {
                    return false;
                }
            }
        }
        true
    }

    // combine search and starts_with
    pub fn search_start_with(&self, prefix: String) -> (bool, bool) {
        let mut curr = self;
        for i in prefix.chars().map(|char| (char as u8 - 'a' as u8) as usize) {
            match curr.nodes[i].as_ref() {
                Some(node) => {
                    curr = node;
                }
                None => {
                    return (false, false);
                }
            }
        }
        (curr.is_end, true)
    }
}
