use crate::os::shell;

pub fn get(url: &str) -> String {
    shell(&format!("curl -s '{}'", url))
}

pub fn post(url: &str, data: &str) -> String {
    shell(&format!("curl -s -X POST -d '{}' '{}'", data, url))
}
