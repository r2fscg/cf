use tokio;

pub async fn sleep(seconds: u64) {
    tokio::time::sleep(tokio::time::Duration::from_secs(seconds)).await;
}

pub async fn sleep_micros(micros: u64) {
    tokio::time::sleep(tokio::time::Duration::from_micros(micros)).await;
}


use reqwest; 
use serde_json;
// async get 
pub async fn get(api:&str)->Result<String, Box<dyn std::error::Error>> {
    Ok(reqwest::Client::new().get(api)
        .send()
        .await?
        .text()
        .await?)
}

// async post
pub async fn post(api:&str, json:&serde_json::Value)->Result<String, Box<dyn std::error::Error>> {
    Ok(reqwest::Client::new().post(api)
        .json(&json)
        .send()
        .await?
        .text()
        .await?)
}

pub async fn form(api:&str, json:&serde_json::Value)->Result<String, Box<dyn std::error::Error>> {
    Ok(reqwest::Client::new().post(api)
        .form(&json)
        .send()
        .await?
        .text()
        .await?)
}


pub async fn form_with_headers(api:&str, json:&serde_json::Value, headers: &reqwest::header::HeaderMap)->Result<String, Box<dyn std::error::Error>> {
    Ok(reqwest::Client::new().post(api)
        .headers(headers.clone())
        .form(&json)
        .send()
        .await?
        .text()
        .await?)
}