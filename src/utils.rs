use std::collections::HashMap;

pub struct SimpleEncoder {
    keys: String,
    values: String,
}

use super::easy_str;

impl SimpleEncoder {
    pub fn new() -> SimpleEncoder {
        let keys="d=Q5?UiS8@f>b]p&/Bu<.lhLzP,$4vMqs-O!;rZ^)HGjk3a*D+}CVR0|I2AE`gm71xcKJYWX(wt9on_%#Ty[NeF6:~{".to_string();
        let values="5xE9&H6){D$cIb|MKtZgoh8O[~>}r<q0ev+a4(s7kp_CySB-=*!]PnfzmV?#WwYAjX%:UJi`.LTNl2^d3/Q,GF@Ru;1".to_string();
        SimpleEncoder { keys, values }
    }

    pub fn transform(&self, key: &str) -> String {
        let key = easy_str::base64_encode(key);
        let hmap: HashMap<char, char> = self.keys.chars().zip(self.values.chars()).collect();
        key.chars()
            .map(|c| hmap.get(&c).cloned().unwrap_or(c))
            .collect()
    }

    pub fn inv(&self, key: &str) -> String {
        let hmap: HashMap<char, char> = self.values.chars().zip(self.keys.chars()).collect();
        let key: String = key
            .chars()
            .map(|c| hmap.get(&c).cloned().unwrap_or(c))
            .collect();
        easy_str::base64_decode(&key)
    }

    pub fn encode(&self, key: &str) -> String {
        self.transform(key)
    }

    pub fn decode(&self, key: &str) -> String {
        self.inv(key)
    }
}

pub fn new_encoder() -> SimpleEncoder {
    SimpleEncoder::new()
}
