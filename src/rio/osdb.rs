use crate::func::str::get_digest;
use std::io::Write;

pub struct Osdb {
    key_path: std::path::PathBuf,
    val_path: std::path::PathBuf,
}

impl Osdb {
    pub fn new(dirpath: std::path::PathBuf) -> Osdb {
        let key_path = dirpath.join("keys");
        if !key_path.exists() {
            std::fs::create_dir(&key_path).expect("Create dir failed.");
        }
        let val_path = dirpath.join("vals");
        if !val_path.exists() {
            std::fs::create_dir(&val_path).expect("Create dir failed.");
        }
        Osdb { key_path, val_path }
    }

    pub fn set(&self, key: &str, value: &str) -> Result<(), String> {
        let digest = get_digest(key);
        let key_path = self.key_path.join(&digest);
        let val_path = self.val_path.join(&digest);
        std::fs::write(key_path, key).expect(&format!("Write file failed: {}", key));
        std::fs::write(val_path, value).expect(&format!("Write file failed: {}", value));
        Ok(())
    }

    pub fn get(&self, key: &str) -> Result<String, String> {
        let digest = get_digest(key);
        let val_path = self.val_path.join(&digest);
        let val_text = std::fs::read_to_string(val_path).expect("Open file failed.");
        Ok(val_text)
    }

    pub fn exists(&self, key: &str) -> bool {
        let digest = get_digest(key);
        let key_path = self.key_path.join(&digest);
        key_path.exists()
    }

    pub fn sadd(&self, set_name: &str, value: &str) -> Result<(), String> {
        // Add a value to set key
        let digest = get_digest(set_name);
        let value_digest = get_digest(value);
        let key_path = self.key_path.join(&digest);
        let mut val_path = self.val_path.join(&digest);
        if !val_path.exists() {
            std::fs::create_dir(&val_path).expect("Create dir failed.");
        }
        val_path.push(&value_digest);

        let mut file = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(key_path)
            .expect(&format!("Open file failed: {}", set_name));
        if let Err(e) = write!(file, "{}", set_name) {
            eprintln!("Couldn't write to file: {}", e);
        }
        let mut file = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(val_path)
            .expect(&format!("Open file failed: {}", value));
        if let Err(e) = write!(file, "{}", value) {
            eprintln!("Couldn't write to file: {}", e);
        }
        Ok(())
    }

    pub fn sdel(&self, key: &str, value: &str) -> Result<(), String> {
        let digest = get_digest(key);
        let value_digest = get_digest(value);
        let target_path = self.val_path.join(&digest).join(&value_digest);
        if target_path.exists() {
            std::fs::remove_file(target_path).expect("Remove file failed.");
        }
        Ok(())
    }

    pub fn smembers(&self, key: &str) -> Result<Vec<String>, String> {
        let digest = get_digest(key);
        let val_path = self.val_path.join(&digest);
        let mut members = Vec::new();
        if val_path.exists() {
            for entry in std::fs::read_dir(val_path).expect("Read dir failed.") {
                let entry = entry.expect("Parse entry failed.");
                let path = entry.path();
                let text = std::fs::read_to_string(path).expect("Open file failed.");
                members.push(text);
            }
        }
        Ok(members)
    }

    pub fn sclear(&self, key: &str) -> Result<(), String> {
        // remove all elements from set and remove path
        let digest = get_digest(key);
        let val_path = self.val_path.join(&digest);
        if val_path.exists() {
            std::fs::remove_dir_all(val_path).expect("Remove dir failed.");
        }
        Ok(())
    }
}
