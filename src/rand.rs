use rand::distributions::Alphanumeric;
use rand::Rng;

pub fn randn() -> i32 {
    // Generate a random number
    let mut rng = rand::thread_rng();
    rng.gen()
}

pub fn rands(len: usize) -> String {
    // Generate a random string with length of length
    let mut rng = rand::thread_rng();
    std::iter::repeat(())
        .map(|()| rng.sample(Alphanumeric))
        .map(char::from)
        .take(len)
        .collect()
}
