pub fn get_digest<S: AsRef<str>>(s: S) -> String {
    use std::hash::{Hash, Hasher};
    let mut hasher = std::collections::hash_map::DefaultHasher::new();
    s.as_ref().hash(&mut hasher);
    format!("{:x}", hasher.finish())
}
