use cf::rio::osdb::Osdb;

fn main() {
    let path = std::path::PathBuf::from("/tmp/rustdb");
    let db = Osdb::new(path);
    db.sadd("hello", "world");
    db.sadd("hello", "good");
    let members = db.smembers("hello").unwrap();

    println!("{:?}", members);
    db.sclear("hello");
    db.set("hello", "world");
    println!("{}", db.exists("hello"));
    // let val = osdb.get("hello").unwrap();
    // println!("{}", val);
}
