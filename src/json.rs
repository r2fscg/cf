use std::collections::HashMap;

pub struct FastJson {}
// @has json.rs/struct.FastJson/index.html
// @has - '//*[@id="method.new"]' 'fn new() -> FastJson'
// @has - '//*[@id="method.new"]' 'Creates a new `FastJson` instance.'
// @has - '//*[@id="method.new"]' '# Examples'
// @has - '//*[@id="method.new"]' '```'
// @has - '//*[@id="method.new"]' 'use json::FastJson;'
impl FastJson {
    pub fn new() -> Self {
        Self {}
    }

    pub fn to_json_string(&self, data: &HashMap<String, String>) -> String {
        let mut json_string = String::new();
        json_string.push_str("{");
        for (key, value) in data {
            json_string.push_str(&format!("\"{}\":\"{}\",", key, value));
        }
        json_string.pop();
        json_string.push_str("}");
        json_string
    }

    fn preprocess_json_string(&self, json_string: &str) -> String {
        json_string
            .replace("{", "")
            .replace("\"", "")
            .replace("}", "")
            .replace(",", "\n")
    }

    pub fn from_str(&self, s: &str) -> Result<HashMap<String, String>, String> {
        let s = &self.preprocess_json_string(s);
        let mut hm = HashMap::new();
        let mut lines = s.lines();
        while let Some(line) = lines.next() {
            let mut parts = line.splitn(2, ':');
            let key = parts.next().unwrap();
            let value = parts.next().unwrap();
            hm.insert(key.to_string(), value.to_string());
        }
        Ok(hm)
    }
}
