use crate::fio::{exists, reads};

/* Difference to sys module:
 * 1. this module provides a wrapper for system command/functionalities
 * 2. sys module provides ways to access system information/data
 */

pub fn sleep(seconds: u64) {
    std::thread::sleep(std::time::Duration::from_secs(seconds));
}

pub fn spawn(cmd: &str) -> (String, String) {
    let stderr_file = "/tmp/RUSTCF_STDERR";
    let cmd = format!("{} 2> {}", cmd, stderr_file);
    let output = std::process::Command::new("sh")
        .arg("-c")
        .arg(cmd)
        .spawn()
        .expect("failed to execute process");
    let resp = output.wait_with_output().expect("failed to wait on child");
    let stdout = String::from_utf8_lossy(&resp.stdout).to_string();
    let stderr = if exists(stderr_file) {
        reads(stderr_file)
    } else {
        String::new()
    };
    (stdout, stderr)
}

pub fn shell(cmd: &str) -> String {
    // Fast dirty way to execute shell command
    let output = std::process::Command::new("sh")
        .arg("-c")
        .arg(cmd)
        .output()
        .expect("failed to execute process");
    let output = String::from_utf8_lossy(&output.stdout);

    output.trim().to_string()
}

pub fn shell2(cmd: &str) -> (String, String) {
    let stderr_file = "/tmp/RUSTCF_STDERR";
    let cmd = format!("{} 2> {}", cmd, stderr_file);
    let output = std::process::Command::new("sh")
        .arg("-c")
        .arg(cmd)
        .output()
        .expect("failed to execute nxshell");
    let stdout = String::from_utf8_lossy(&output.stdout).to_string();
    let stderr = if exists(stderr_file) {
        reads(stderr_file)
    } else {
        String::new()
    };
    (stdout, stderr)
}
