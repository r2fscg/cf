pub struct TextFormat {}
impl TextFormat {
    /// Format text with color or certain style.
    pub fn new() -> TextFormat {
        TextFormat {}
    }

    /// Add color to text.
    pub fn colored(&self, text: &str, color: &str) -> String {
        match color {
            "red" => format!("\x1b[31m{}\x1b[0m", text),
            "green" => format!("\x1b[32m{}\x1b[0m", text),
            "yellow" => format!("\x1b[33m{}\x1b[0m", text),
            "blue" => format!("\x1b[34m{}\x1b[0m", text),
            "magenta" => format!("\x1b[35m{}\x1b[0m", text),
            "cyan" => format!("\x1b[36m{}\x1b[0m", text),
            "white" => format!("\x1b[37m{}\x1b[0m", text),
            "light_red" => format!("\x1b[91m{}\x1b[0m", text),
            "light_green" => format!("\x1b[92m{}\x1b[0m", text),
            "light_yellow" => format!("\x1b[93m{}\x1b[0m", text),
            "light_blue" => format!("\x1b[94m{}\x1b[0m", text),
            "light_magenta" => format!("\x1b[95m{}\x1b[0m", text),
            "light_cyan" => format!("\x1b[96m{}\x1b[0m", text),
            "light_white" => format!("\x1b[97m{}\x1b[0m", text),
            _ => text.to_string(),
        }
    }
}
