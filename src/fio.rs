use crate::log::logger::SimpleLogger;
use dotenv;
use std::env;
use std::path::Path;

pub fn get_key(env_path: &'static str, var_name: &str) -> String {
    // Get key from .env file, env_path is the path to .env file, e.g., "/tmp/myenv"
    let env_path = Path::new(env_path);
    let env = dotenv::from_path(env_path);
    env.as_ref().unwrap();
    env::var(var_name).unwrap()
}

pub fn basename(path: &str) -> String {
    let path = std::path::Path::new(path);
    path.file_name().unwrap().to_str().unwrap().to_string()
}

pub fn size(path: &str) -> u64 {
    std::fs::metadata(path).unwrap().len()
}

pub fn reads(fpath: &str) -> String {
    if !std::path::Path::new(fpath).exists() {
        let log = SimpleLogger::new();
        log.error(&format!("File not found: {}", fpath));
        return String::new();
    }
    let text = std::fs::read_to_string(fpath).expect("Open file failed.");
    text
}

pub fn writes(fpath: &str, text: &str) {
    // Fast diry way to write file
    std::fs::write(fpath, text).expect("Write file failed.");
}

pub fn exists(fpath: &str) -> bool {
    let path = std::path::Path::new(fpath);
    path.exists()
}

pub fn rm(fpath: &str) {
    std::fs::remove_file(fpath).expect("Remove file failed.");
}

pub fn remove(fpath: &str) {
    std::fs::remove_file(fpath).expect("Remove file failed.");
}
