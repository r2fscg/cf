use core::fmt::Debug;
use log::logger::SimpleLogger;
use std::io::{prelude::*, BufReader};
use std::{fs, io, path::Path, path::PathBuf};
pub mod aperedis;
pub mod asyncio;
pub mod color;
pub mod curl;
pub mod easy_str;
pub mod fio;
pub mod func;
pub mod json;
pub mod log;
pub mod net;
pub mod os;
pub mod rand;
pub mod rio;
pub mod sys;
pub mod time;
pub mod utils;

use lazy_static::lazy_static;
lazy_static! {
    pub static ref LOG: SimpleLogger = SimpleLogger::new();
}

pub fn simple_decode(s: &str) -> String {
    // quick api to simple encoer
    use utils::SimpleEncoder;
    SimpleEncoder::new().inv(s)
}

// SimpleLogger instance api
pub fn get_logger() -> &'static SimpleLogger {
    &LOG
}

pub fn log() -> &'static SimpleLogger {
    &LOG
}

pub fn print(msg: &str) {
    println!("{}", msg);
}

pub fn say(msg: &str) {
    println!("{}", msg);
}

pub fn pt(msg: &str) {
    println!("{}", msg);
}

pub struct IO {}
impl IO {
    pub fn say<T: Debug>(e: &T) {
        /* Display data. */
        println!("{:?}", e);
    }

    pub fn pt(text: &str) {
        println!("{}", text);
    }

    pub fn lines_from_file(filename: impl AsRef<Path>) -> Vec<String> {
        let file = std::fs::File::open(filename).expect("Open file failed.");
        let buf = BufReader::new(file);
        buf.lines()
            .map(|l| l.expect("Could not parse line."))
            .collect()
    }
    pub fn trim_reads(fpath: &str) -> String {
        // Also trim the newline
        let text = fio::reads(fpath);
        text.trim().to_string()
    }

    #[deprecated = "deprecated in favor of cf::fio::reads"]
    pub fn reads(fpath: &str) -> String {
        if !Path::new(fpath).exists() {
            LOG.error(&format!("File not found: {}", fpath));
            return String::new();
        }
        let text = std::fs::read_to_string(fpath).expect("Open file failed.");
        text
    }

    #[deprecated = "deprecated in favor of cf::fio::exists"]
    pub fn exists(fpath: &str) -> bool {
        let path = Path::new(fpath);
        path.exists()
    }

    #[deprecated = "deprecated in favor of cf::fio::writes"]
    pub fn writes(fpath: &str, text: &str) {
        // Fast diry way to write file
        fs::write(fpath, text).expect("Write file failed.");
    }
}

pub fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}

pub struct FileSystem {}

impl FileSystem {
    pub fn walk<P>(dir_name: P) -> Vec<String>
    where
        P: AsRef<Path>,
    {
        // Walk over a directory.
        std::fs::read_dir(dir_name)
            .expect("read dir failed")
            .map(|entry| {
                entry
                    .expect("parse extry failed")
                    .path()
                    .to_str()
                    .expect("to str failed")
                    .to_string()
            })
            .collect()
    }

    pub fn read_to_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<std::fs::File>>>
    where
        P: AsRef<Path>,
    {
        // Read file contents into vector of string
        let file = std::fs::File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }

    pub fn read_to_string<P>(filename: P) -> io::Result<String>
    where
        P: AsRef<Path>,
    {
        // A wrapper of fs::read_to_string.
        fs::read_to_string(filename)
    }

    pub fn read_to_vector<P>(filename: P) -> Vec<String>
    where
        P: AsRef<Path>,
    {
        let fh = std::fs::File::open(filename).expect("failed to open file");
        std::io::BufReader::new(fh)
            .lines()
            .map(|ln| ln.expect("read line failed"))
            .collect()
    }

    // ------- write operations
    pub fn write_iterator_to_file<T>(file_name: &str, stream: T)
    where
        T: IntoIterator<Item = String>,
    {
        let strvec = stream.into_iter().collect::<Vec<String>>();
        std::fs::write(file_name, strvec.join("\n"))
            .expect(&format!("Failed to write contents to file {}", file_name));
    }

    pub fn cp(src: &str, target: &str) {
        fs::copy(src, target).expect(&format!("Failed to COPY file {} to {}", src, target));
    }

    pub fn rm(filename: &str) {
        fs::remove_file(filename).expect(&format!("Failed to REMOVE file {}", filename));
    }

    pub fn exists(filename: &str) -> bool {
        // Does file exist or not.
        Path::new(filename).exists()
    }

    pub fn dir_size(path: impl Into<PathBuf>) -> io::Result<u64> {
        // Get directory size.
        fn rec(mut dir: fs::ReadDir) -> io::Result<u64> {
            dir.try_fold(0, |acc, file| {
                let file = file?;
                let size = match file.metadata()? {
                    data if data.is_dir() => rec(fs::read_dir(file.path())?)?,
                    data => data.len(),
                };
                Ok(acc + size)
            })
        }

        let buf = path.into();
        let f = fs::metadata(&buf).unwrap();
        if f.is_file() {
            return Ok(f.len());
        }
        rec(fs::read_dir(&buf)?)
    }
}
