use std::collections::HashMap;
use std::process::Command;
/*
 * Get Linux/MacOS system basic information with std::process::Command.
 * @author: GaoangLau 2022-01-05
 */
pub struct SysInfo {
    hostname: String,
    username: String,
    ip: String,
    os: String,
    kernel: String,
    arch: String,
    cpu: String,
    mem: String,
    uptime: String,
    load: String,
    disk: String,
    net: String,
    mac_addr: String,
    initialized: bool,
}

impl SysInfo {
    pub fn new() -> SysInfo {
        SysInfo {
            hostname: String::new(),
            username: String::new(),
            ip: String::new(),
            os: String::new(),
            kernel: String::new(),
            arch: String::new(),
            cpu: String::new(),
            mem: String::new(),
            uptime: String::new(),
            load: String::new(),
            disk: String::new(),
            net: String::new(),
            mac_addr: String::new(),
            initialized: false,
        }
    }

    fn init(&mut self) {
        if !self.initialized {
            // Initialize system information only once
            self.get_hostname();
            self.get_username();
            self.get_ip();
            self.get_os();
            self.get_kernel();
            self.get_arch();
            self.get_cpu();
            self.get_mem();
            self.get_uptime();
            self.get_load();
            self.get_disk();
            self.get_net();
            self.get_mac_addr();
            self.initialized = true;
        }
    }

    pub fn __dict__(&mut self) -> HashMap<&str, String> {
        self.init();
        let mut hm = HashMap::new();
        hm.insert("hostname", self.hostname.clone());
        hm.insert("username", self.username.clone());
        hm.insert("ip", self.ip.clone());
        hm.insert("os", self.os.clone());
        hm.insert("kernel", self.kernel.clone());
        hm.insert("arch", self.arch.clone());
        hm.insert("cpu", self.cpu.clone());
        hm.insert("mem", self.mem.clone());
        hm.insert("uptime", self.uptime.clone());
        hm.insert("load", self.load.clone());
        hm.insert("disk", self.disk.clone());
        hm.insert("net", self.net.clone());
        hm.insert("mac_addr", self.mac_addr.clone());
        hm
    }

    pub fn __str__(&mut self) -> String {
        let map = self.__dict__();
        let mut keys = map.keys().map(|&x| x).collect::<Vec<&str>>();
        keys.sort();
        keys.into_iter().fold(String::new(), |mut acc, x| {
            acc.push_str(&format!("{:16}: {}\n", x, map[x]));
            acc
        })
    }

    fn run_command(&mut self, cmd: &str) -> String {
        let output = Command::new("sh")
            .arg("-c")
            .arg(cmd)
            .output()
            .expect("failed to execute process");
        String::from_utf8_lossy(&output.stdout).trim().to_string()
    }

    fn get_username(&mut self) {
        self.username = self.run_command("whoami");
    }

    fn get_mac_addr(&mut self) -> String {
        let cmd = "ifconfig | grep -Eo '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}' | sort | uniq | tail -n 1";
        self.mac_addr = self.run_command(cmd);
        self.mac_addr.clone()
    }

    fn get_hostname(&mut self) {
        self.hostname = self.run_command("hostname");
    }

    fn get_ip(&mut self) {
        self.ip = self.run_command(
            "ip addr show | grep \"inet \" | awk '{print $2}' | grep 192 | cut -d/ -f1",
        );
    }

    fn get_os(&mut self) {
        self.os = self.run_command("uname");
    }

    fn get_kernel(&mut self) {
        self.kernel = self.run_command("uname -r");
    }

    fn get_arch(&mut self) {
        self.arch = self.run_command("uname -m");
    }

    fn get_cpu(&mut self) {
        let os = self.os.clone();
        self.cpu = match os.as_str() {
            "Linux" => self.run_command("cat /proc/cpuinfo | grep \"model name\" | head -n 1 | cut -d: -f2 | sed 's/^ *//g'"),
            "Darwin" => self.run_command("sysctl -n machdep.cpu.brand_string"),
            _ => String::new(),
        };
    }

    fn get_mem(&mut self) {
        self.mem = self.run_command("free -m | grep Mem | awk '{print $2}'");
    }

    fn get_uptime(&mut self) {
        self.uptime = self.run_command("uptime -p");
    }

    fn get_load(&mut self) {
        self.load = self.run_command("cat /proc/loadavg");
    }

    fn get_disk(&mut self) {
        // self.disk = self.run_command("df -h");
        self.disk = "NotImplemented".to_string();
    }

    fn get_net(&mut self) {
        self.net = "NotImplemented".to_string();
        // self.net = self.run_command("ifconfig");
    }
}
