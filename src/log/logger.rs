use crate::color::TextFormat;
use chrono;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;

const MAX_LOG_SIZE: u64 = 100 * 1024 * 1024;

pub struct SimpleLogger {
    logfile: String,
    text_formatter: TextFormat,
}

impl SimpleLogger {
    pub fn new() -> SimpleLogger {
        SimpleLogger {
            logfile: "/tmp/rustlog.txt".to_string(),
            text_formatter: TextFormat::new(),
        }
    }

    fn init(&self) {
        let path = Path::new(&self.logfile);
        if path.exists() {
            if path.metadata().unwrap().len() > MAX_LOG_SIZE {
                std::fs::rename(&self.logfile, &format!("{}.old", self.logfile))
                    .expect("rename failed");
                std::fs::File::create(&self.logfile).expect("create failed");
            }
        } else {
            let mut file = OpenOptions::new()
                .write(true)
                .create(true)
                .open(&self.logfile)
                .expect("create new log failed");
            file.write_all(b"<create Rust SimpleLogger>\n")
                .expect("write to file error");
        }
    }

    pub fn _stream(&self, level: &str, msg: &str, color: &str) {
        let s = format!(
            "[{}] [{}]: {}",
            chrono::Local::now().format("%Y-%m-%d %H:%M:%S.%3f"),
            level,
            self.text_formatter.colored(msg, color)
        );
        println!("{}", s);
        self.init();

        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(&self.logfile)
            .expect("open logger file error");
        if let Err(e) = writeln!(file, "{}", s) {
            eprintln!("Couldn't write to file: {}", e);
        }
    }
    pub fn info(&self, msg: &str) {
        self._stream("INFO", msg, "light_cyan");
    }

    pub fn warn(&self, msg: &str) {
        self._stream("WARN", msg, "light_yellow");
    }
    pub fn warning(&self, msg: &str) {
        self._stream("WARN", msg, "light_yellow");
    }
    pub fn error(&self, msg: &str) {
        self._stream("ERROR", msg, "light_red");
    }
}
