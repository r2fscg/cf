pub mod logger;

fn getlogger() -> logger::SimpleLogger {
    logger::SimpleLogger::new()
}

pub fn info(msg: &str) {
    getlogger().info(msg);
}

pub fn error(msg: &str) {
    getlogger().error(msg);
}

pub fn warning(msg: &str) {
    getlogger().warning(msg);
}

pub fn warn(msg: &str) {
    getlogger().warn(msg);
}

pub struct CIA {}

impl CIA {
    pub fn new() -> CIA {
        CIA {}
    }
    pub fn info(msg: &str) {
        info(msg);
    }
    pub fn warning(msg: &str) {
        warning(msg);
    }
    pub fn warn(msg: &str) {
        warn(msg);
    }
    pub fn error(msg: &str) {
        error(msg);
    }
}
