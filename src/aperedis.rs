use crate::utils::SimpleEncoder;
use redis::Commands;

pub struct ApeRedis {
    pub client: redis::Connection,
}

impl ApeRedis {
    pub fn new<S: AsRef<str> + std::fmt::Display>(host: S, port: u16, password: S) -> Self {
        let client =
            redis::Client::open(format!("redis://default:{}@{}:{}/", password, host, port))
                .expect("falied to open url")
                .get_connection()
                .expect("failed to connection");
        ApeRedis { client }
    }

    pub fn ping(&mut self) -> String {
        self.get("ping")
    }

    pub fn rpush(&mut self, list_name: &str, value: &str) -> Result<i64, redis::RedisError> {
        redis::cmd("RPUSH")
            .arg(list_name)
            .arg(value)
            .query(&mut self.client)
    }

    pub fn set(&mut self, key: &str, value: &str) -> bool {
        match redis::cmd("SET")
            .arg(key)
            .arg(value)
            .query::<String>(&mut self.client)
        {
            Ok(_) => true,
            Err(e) => {
                println!("{:?}", e);
                false
            }
        }
    }

    pub fn set_expire(&mut self, key: &str, value: &str, expire: u32) -> bool {
        // Expire in seconds
        match redis::cmd("SET")
            .arg(key)
            .arg(value)
            .arg("EX")
            .arg(expire)
            .query::<String>(&mut self.client)
        {
            Ok(_) => true,
            Err(e) => {
                println!("{:?}", e);
                false
            }
        }
    }

    pub fn get(&mut self, key: &str) -> String {
        match redis::cmd("GET").arg(key).query::<String>(&mut self.client) {
            Ok(s) => s,
            Err(e) => {
                println!("{:?}", e);
                String::new()
            }
        }
    }
    pub fn exists(&mut self, key: &str) -> bool {
        match redis::cmd("EXISTS").arg(key).query::<i32>(&mut self.client) {
            Ok(s) => s > 0,
            Err(e) => {
                println!("{:?}", e);
                false
            }
        }
    }

    pub fn del(&mut self, key: &str) -> bool {
        match redis::cmd("DEL").arg(key).query::<String>(&mut self.client) {
            Ok(_) => true,
            Err(e) => {
                println!("{:?}", e);
                false
            }
        }
    }

    pub fn blpop(
        &mut self,
        key: &str,
        timeout: u32,
    ) -> Result<(String, String), redis::RedisError> {
        redis::cmd("BLPOP")
            .arg(key)
            .arg(timeout)
            .query(&mut self.client)
    }
}

// Another version of myredis
pub struct MyRedis {
    pub client: Result<redis::Client, redis::RedisError>,
}

#[derive(Debug, Clone, Copy)]
pub enum RedisResults {
    GetError,
    PushError,
    SetError,
    SetExpireError,
    BlpopError,
}

impl std::fmt::Display for RedisResults {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            RedisResults::GetError => write!(f, "GET_ERROR"),
            RedisResults::PushError => write!(f, "PUSH_ERROR"),
            RedisResults::SetError => write!(f, "SET_ERROR"),
            RedisResults::SetExpireError => write!(f, "SET_EXPIRE_ERROR"),
            RedisResults::BlpopError => write!(f, "BLPOP_ERROR"),
        }
    }
}

impl MyRedis {
    pub fn new<S: AsRef<str> + std::fmt::Display>(host: S, port: u16, password: S) -> Self {
        let full_host = format!("redis://default:{}@{}:{}/", password, host, port);
        let client = redis::Client::open(full_host);
        MyRedis { client }
    }

    pub fn get(&mut self, key: &str) -> String {
        if let Ok(client) = self.client.as_mut() {
            match client.get(key) {
                Ok(s) => {
                    if let Some(s) = s {
                        s
                    } else {
                        String::from("")
                    }
                }
                Err(e) => {
                    println!("{:?}", e);
                    RedisResults::GetError.to_string()
                }
            }
        } else {
            RedisResults::GetError.to_string()
        }
    }

    pub fn set(&mut self, key: &str, value: &str) -> bool {
        if let Ok(client) = self.client.as_mut() {
            match client.set::<&str, &str, ()>(key, value) {
                Ok(_) => true,
                Err(e) => {
                    println!("{:?}", e);
                    false
                }
            }
        } else {
            false
        }
    }

    pub fn set_expire(&mut self, key: &str, value: &str, expire: usize) -> bool {
        if let Ok(client) = self.client.as_mut() {
            match client.set_ex::<&str, &str, ()>(key, value, expire) {
                Ok(_) => true,
                Err(e) => {
                    println!("MyRedis setexpire error: {:?}", e);
                    false
                }
            }
        } else {
            false
        }
    }

    pub fn rpush(&mut self, key: &str, value: &str) -> bool {
        if let Ok(client) = self.client.as_mut() {
            match client.rpush::<&str, &str, ()>(key, value) {
                Ok(_) => true,
                Err(e) => {
                    println!("{:?}", e);
                    false
                }
            }
        } else {
            false
        }
    }

    pub fn blpop(&mut self, key: &str, timeout: usize) -> String {
        if let Ok(client) = self.client.as_mut() {
            match client.blpop::<&str, (String, String)>(key, timeout) {
                Ok((_, s)) => s.to_string(),
                Err(e) => {
                    println!("{:?}", e);
                    RedisResults::BlpopError.to_string()
                }
            }
        } else {
            RedisResults::BlpopError.to_string()
        }
    }
}

pub fn redis_jp() -> Result<ApeRedis, redis::RedisError> {
    let password = "_)_z2VLV4GOFV|EUqGQN_qGEy2m4$TZG";
    let host = "w60orXT4S20o%lw";
    let c = SimpleEncoder::new();
    Ok(ApeRedis::new(c.inv(host), 6379, c.inv(password)))
}

pub fn redis_jp2() -> Result<redis::Connection, redis::RedisError> {
    let c = SimpleEncoder::new();
    let (host, port, password) = ("w60orXT4S20o%lw", 31216, "_)_z2VLV4GOFV|EUqGQN_qGEy2m4$TZG");
    let rcli = redis::Client::open(format!("redis://:{}@{}:{}/0", c.inv(password), host, port))?;
    rcli.get_connection()
}
