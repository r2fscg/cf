pub fn now() -> String {
    // human readable time, you wont say now is 2020-12-12 12:12:12 in real conversation
    let now = chrono::Local::now();
    let time = now.format("%H:%M").to_string();
    time
}

pub fn time() -> String {
    let now = chrono::Local::now();
    let time = now.format("%H:%M:%S").to_string();
    time
}

pub fn date() -> String {
    chrono::Local::now().format("%Y-%m-%d %H:%M:%S").to_string()
}

pub fn datez() -> String {
    // Date with zone
    chrono::Local::now()
        .format("%Y-%m-%d %H:%M:%S%z")
        .to_string()
}
