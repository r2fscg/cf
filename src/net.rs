use crate::os::shell;
use reqwest;
use serde_json;

pub fn get(url: &str) -> String {
    reqwest::blocking::get(url)
        .expect("failed to get")
        .text()
        .expect("failed to get")
}

pub fn is_in_china() -> bool {
    // Check if the current machine is in china.
    let country = shell("curl -s -m3 http://ipinfo.io/country");
    country == "CN" || country == ""
}

// async get 
pub async fn aget(api:&str)->Result<String, Box<dyn std::error::Error>> {
    Ok(reqwest::Client::new().get(api)
        .send()
        .await?
        .text()
        .await?)
}

// async post
pub async fn apost(api:&str, json:&serde_json::Value)->Result<String, Box<dyn std::error::Error>> {
    Ok(reqwest::Client::new().post(api)
        .json(&json)
        .send()
        .await?
        .text()
        .await?)
}

// sync post 
pub fn post(api:&str, json:&serde_json::Value)->Result<String, Box<dyn std::error::Error>> {
    Ok(reqwest::blocking::Client::new().post(api)
        .json(&json)
        .send()?
        .text()?)
}
