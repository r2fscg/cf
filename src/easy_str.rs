use base64;
// pub use crate::os::shell;

pub fn base64_encode(s: &str) -> String {
    base64::encode(s)
}

pub fn base64_decode(s: &str) -> String {
    if let Ok(s) = base64::decode(s) {
        if let Ok(s) = String::from_utf8(s) {
            s.trim().to_string()
        } else {
            String::new()
        }
    } else {
        String::new()
    }
}
